import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as bcrypt from 'bcrypt';
import { Envs } from '../constants/envs.const';

@Injectable()
export class HashingService {
  constructor(private configService: ConfigService) {}

  private readonly saltRounds = this.configService.get<number>(
    Envs.SALT_ROUNDS
  );

  async hashPassword(password: string): Promise<string> {
    const salt = await bcrypt.genSalt(this.saltRounds);
    return bcrypt.hash(password, salt);
  }

  async comparePasswords(
    password: string,
    storedPasswordHash: string
  ): Promise<boolean> {
    return bcrypt.compare(password, storedPasswordHash);
  }
}
