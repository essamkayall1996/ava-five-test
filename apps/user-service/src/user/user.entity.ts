import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { UserRoles } from '../constants/UserRoles.enum';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ type: 'enum', enum: UserRoles, default: UserRoles.USER })
  role: UserRoles;
}
