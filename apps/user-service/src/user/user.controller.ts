import { Controller, Body } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateNewUserDTO, LoginUserDTO } from './dto';
import { MessagePattern } from '@nestjs/microservices';
import { MeDTO } from './dto/me.dto';
import { PaginationDto } from '../common/dto/pagination.dto';

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @MessagePattern({ cmd: 'register' })
  async register(@Body() createUserDto: CreateNewUserDTO) {
    return this.userService.register(createUserDto);
  }

  @MessagePattern({ cmd: 'login' })
  async login(@Body() loginUserDto: LoginUserDTO) {
    return this.userService.login(loginUserDto);
  }

  @MessagePattern({ cmd: 'list' })
  async getList(@Body() paginationDto: PaginationDto) {
    return this.userService.getList(paginationDto);
  }

  @MessagePattern({ cmd: 'me' })
  async me(@Body() meDTO: MeDTO) {
    return this.userService.me(meDTO);
  }
}
