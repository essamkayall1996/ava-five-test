import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class MeDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  email: string;
}
