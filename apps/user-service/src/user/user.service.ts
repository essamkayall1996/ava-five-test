import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { CreateNewUserDTO, LoginUserDTO } from './dto';
import { HashingService } from '../services/hash.service';
import { RpcException } from '@nestjs/microservices';
import { UserErrors } from '../errors/user-errors';
import { JwtService } from '@nestjs/jwt';
import { MeDTO } from './dto/me.dto';
import { PaginationDto } from '../common/dto/pagination.dto';
import { ConfigService } from '@nestjs/config';
import { Envs } from '../constants/envs.const';
import { UserRoles } from '../constants/UserRoles.enum';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private hashingService: HashingService,
    private jwtService: JwtService,
    private configService: ConfigService
  ) {}

  async register(
    createUserDto: CreateNewUserDTO
  ): Promise<Partial<User> & { access_token: string }> {
    const _user = await this.userRepository.findOne({
      where: { email: createUserDto.email },
    });

    if (_user) throw new RpcException(UserErrors.UserEmailAlreadyInUse);

    const hashedPassword = await this.hashingService.hashPassword(
      createUserDto.password
    );

    const user = this.userRepository.create({
      email: createUserDto.email,
      password: hashedPassword,
    });

    const access_token = this.signingUser(user.email, user.role, user.id);

    await this.userRepository.save(user);

    const { password, ...other } = user;

    return { ...other, access_token: access_token };
  }

  async login(
    loginUserDto: LoginUserDTO
  ): Promise<Partial<User> & { access_token: string }> {
    const _user = await this.userRepository.findOne({
      where: { email: loginUserDto.email },
    });

    if (!_user) throw new RpcException(UserErrors.InvalideEmailOrPassword);

    if (
      !(await this.hashingService.comparePasswords(
        loginUserDto.password,
        _user.password
      ))
    )
      throw new RpcException(UserErrors.InvalideEmailOrPassword);

    const access_token = this.signingUser(_user.email, _user.role, _user.id);

    return { email: loginUserDto.email, access_token: access_token };
  }

  async getList(
    paginationDto: PaginationDto
  ): Promise<{ data: User[]; total: number }> {
    const { page, perPage } = paginationDto;

    const [data, total] = await this.userRepository.findAndCount({
      select: { email: true, id: true, role: true },
      skip: page * perPage,
      take: perPage,
    });

    return { data, total };
  }

  async me(meDto: MeDTO): Promise<User> {
    return await this.userRepository.findOne({
      where: { email: meDto.email },
      select: { email: true, id: true, role: true },
    });
  }

  private signingUser(email: string, role: UserRoles, id: number): string {
    return this.jwtService.sign(
      {
        email: email,
        role: role,
        id: id,
      },
      {
        secret: this.configService.get<string>(Envs.JWT_SECRET),
        expiresIn: this.configService.get<number>(Envs.JWT_EXPIRY),
      }
    );
  }
}
