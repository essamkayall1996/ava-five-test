export class UserErrors {
  public static UserEmailAlreadyInUse = {
    errorCode: 409,
    message: 'Email already exists',
  };

  public static InvalideEmailOrPassword = {
    errorCode: 400,
    message: 'Invalide email or password',
  };
}
