import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Envs } from './constants/envs.const';
import { validate } from './config/config.validation';

@Module({
  imports: [
    ConfigModule.forRoot({
      validate: validate,
      isGlobal: true,
      envFilePath: 'user-service/.env',
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get<string>(Envs.DB_HOST),
        port: configService.get<number>(Envs.DB_PORT),
        username: configService.get<string>(Envs.DB_USERNAME),
        password: configService.get<string>(Envs.DB_PASSWORD),
        autoLoadEntities: true,
      }),
    }),
    UserModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>(Envs.JWT_SECRET),
        signOptions: {
          expiresIn: `${configService.get<string>(Envs.JWT_SECRET)}s`,
        },
      }),
    }),
  ],
})
export class AppModule {}
