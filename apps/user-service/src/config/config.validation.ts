import { Type, plainToInstance } from 'class-transformer';
import { IsNumber, IsString, validateSync } from 'class-validator';
import { Envs } from '../constants/envs.const';

class EnvironmentVariables {
  @IsString()
  @Type(() => String)
  [Envs.HOST]!: string;

  @IsNumber()
  @Type(() => Number)
  [Envs.PORT]!: number;

  // JWT
  @IsString()
  @Type(() => String)
  [Envs.JWT_SECRET]!: string;

  @IsNumber()
  @Type(() => Number)
  [Envs.JWT_EXPIRY]!: number;

  // HASHING
  @IsNumber()
  @Type(() => Number)
  [Envs.SALT_ROUNDS]!: number;

  // DATABASE
  @IsString()
  @Type(() => String)
  [Envs.DB_HOST]!: string;

  @IsString()
  @Type(() => String)
  [Envs.DB_PASSWORD]!: string;

  @IsNumber()
  @Type(() => Number)
  [Envs.DB_PORT]!: number;

  @IsString()
  @Type(() => String)
  [Envs.DB_USERNAME]!: string;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToInstance(EnvironmentVariables, config);

  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }

  return validatedConfig;
}
