import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { Envs } from './constants/envs.const';

async function bootstrap() {
  const appContext = await NestFactory.createApplicationContext(AppModule);

  const configService: ConfigService = appContext.get(ConfigService);
  const PORT = configService.get<number>(Envs.PORT);
  const HOST = configService.get<string>(Envs.HOST);

  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.TCP,
      options: {
        host: HOST,
        port: PORT,
      },
    }
  );

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
      disableErrorMessages: false,
    })
  );

  app
    .listen()
    .then(() => console.log('Product Service is listening'))
    .catch((e) => console.error(e));
}
bootstrap();
