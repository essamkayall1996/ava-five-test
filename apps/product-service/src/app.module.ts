import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductModule } from './product/product.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { validate } from './config/config.validation';
import { Envs } from './constants/envs.const';

@Module({
  imports: [
    ConfigModule.forRoot({
      validate: validate,
      isGlobal: true,
      envFilePath: 'product-service/.env',
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get<string>(Envs.DB_HOST),
        port: configService.get<number>(Envs.DB_PORT),
        username: configService.get<string>(Envs.DB_USERNAME),
        password: configService.get<string>(Envs.DB_PASSWORD),
        autoLoadEntities: true,
      }),
    }),
    ProductModule,
  ],
})
export class AppModule {}
