export const Envs = Object.freeze({
  PORT: 'PORT',
  HOST: 'HOST',
  DB_HOST: 'DB_HOST',
  DB_PORT: 'DB_PORT',
  DB_USERNAME: 'DB_USERNAME',
  DB_PASSWORD: 'DB_PASSWORD',
} as const);
