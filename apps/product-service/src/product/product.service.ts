import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { Product } from './product.entity';
import { CreateNewProductDTO, ListProductsDto } from './dto';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>
  ) {}

  async create(createNewProductDTO: CreateNewProductDTO): Promise<Product> {
    const product = this.productRepository.create(createNewProductDTO);

    await this.productRepository.save(product);

    return product;
  }

  async getProducts(
    listProductsDto: ListProductsDto
  ): Promise<{ data: Product[]; total: number }> {
    const { order, page, perPage, sort, name, userId } = listProductsDto;

    const [data, total] = await this.productRepository.findAndCount({
      where: {
        ...(name && {
          productName: ILike(`%${name}%`),
        }),
        ...(userId && { userId }),
      },
      order: { [`${sort}`]: order },
      skip: page * perPage,
      take: perPage,
    });

    return { data, total };
  }
}
