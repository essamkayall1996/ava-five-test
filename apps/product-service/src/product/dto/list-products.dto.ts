import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { PaginationDto } from '../../common/dto/pagination.dto';
import { ApiProperty } from '@nestjs/swagger';

export enum SortOrder {
  ASC = 'ASC',
  DESC = 'DESC',
}

export enum SortingKeys {
  PRICE = 'price',
  ID = 'id',
  PRODUCT_NAME = 'productName',
}

export class ListProductsDto extends PaginationDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  userId?: number;

  @ApiProperty()
  @IsOptional()
  @IsEnum(SortOrder)
  order: SortOrder = SortOrder.ASC;

  @ApiProperty()
  @IsOptional()
  @IsEnum(SortingKeys)
  sort = SortingKeys.PRICE;
}
