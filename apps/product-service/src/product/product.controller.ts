import { Body, Controller } from '@nestjs/common';
import { ProductService } from './product.service';
import { MessagePattern } from '@nestjs/microservices';
import { CreateNewProductDTO, ListProductsDto } from './dto';

@Controller('products')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @MessagePattern({ cmd: 'create' })
  async register(@Body() createUserDto: CreateNewProductDTO) {
    return this.productService.create(createUserDto);
  }

  @MessagePattern({ cmd: 'list-products' })
  async listProducts(@Body() listProductsDto: ListProductsDto) {
    return this.productService.getProducts(listProductsDto);
  }
}
