import {
  Controller,
  Post,
  Body,
  Inject,
  UseGuards,
  Get,
  Query,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateNewProductDTO, ListProductsDto } from './dto';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { CurrentUser } from '../common/decorators/current-user.decorator';
import { ServicesNames } from '../constants/ServicesNames.const';
import { ICurrentUser } from '../common/@types/current-user.type';

@Controller('product')
export class ProductController {
  constructor(
    @Inject(ServicesNames.PRODUCT_SERVICE.NAME)
    private readonly client: ClientProxy
  ) {}

  @Post('create')
  @UseGuards(JwtAuthGuard)
  async register(
    @CurrentUser() user: ICurrentUser,
    @Body() createNewProductDTO: CreateNewProductDTO
  ) {
    return this.client.send(
      { cmd: 'create' },
      { ...createNewProductDTO, userId: user.id }
    );
  }

  @Get('list')
  @UseGuards(JwtAuthGuard)
  async getProducts(
    @CurrentUser() user: ICurrentUser,
    @Query() listProductsDto: ListProductsDto
  ) {
    return this.client.send(
      { cmd: 'list-products' },
      { ...listProductsDto, userId: user.id }
    );
  }
}
