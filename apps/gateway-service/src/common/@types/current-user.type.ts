import { UserRoles } from '../../constants/UserRoles.enum';

export interface ICurrentUser {
  id: number;
  email: string;
  role: UserRoles;
}
