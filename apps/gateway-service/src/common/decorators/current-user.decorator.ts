import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { ICurrentUser } from '../@types/current-user.type';

export const CurrentUser = createParamDecorator(
  async (data: unknown, context: ExecutionContext): Promise<ICurrentUser> => {
    const request = context.switchToHttp().getRequest();
    return request?.user;
  }
);
