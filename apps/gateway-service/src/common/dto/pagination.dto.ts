import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';

export class PaginationDto {
  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  page = 0;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  perPage = 10;
}
