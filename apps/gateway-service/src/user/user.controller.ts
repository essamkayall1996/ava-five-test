import {
  Controller,
  Post,
  Body,
  Inject,
  ConflictException,
  UnauthorizedException,
  Get,
  UseGuards,
  Query,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateNewUserDTO, LoginUserDTO } from './dto';
import { catchError, throwError } from 'rxjs';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { RolesGuard } from '../common/guards/role.guard';
import { Roles } from '../common/decorators/roles.decorator';
import { UserRoles } from '../constants/UserRoles.enum';
import { CurrentUser } from '../common/decorators/current-user.decorator';
import { PaginationDto } from '../common/dto/pagination.dto';
import { ServicesNames } from '../constants/ServicesNames.const';
import { ICurrentUser } from '../common/@types/current-user.type';
import { ApiBearerAuth } from '@nestjs/swagger';

@ApiBearerAuth()
@Controller('users')
export class UserController {
  constructor(
    @Inject(ServicesNames.USER_SERVICE.NAME)
    private readonly client: ClientProxy
  ) {}

  @Post('register')
  async register(@Body() createUserDto: CreateNewUserDTO) {
    return this.client.send({ cmd: 'register' }, createUserDto).pipe(
      catchError((error) => {
        return throwError(() => {
          if (error.errorCode === 409)
            throw new ConflictException(error.message);
        });
      })
    );
  }

  @Post('login')
  async login(@Body() loginUserDto: LoginUserDTO) {
    return this.client.send({ cmd: 'login' }, loginUserDto).pipe(
      catchError((error) => {
        return throwError(() => {
          if (error.errorCode === 400)
            throw new UnauthorizedException(error.message);
        });
      })
    );
  }

  @Get('me')
  @UseGuards(JwtAuthGuard)
  async me(@CurrentUser() user: ICurrentUser) {
    return this.client.send({ cmd: 'me' }, { email: user.email });
  }

  @Roles(UserRoles.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('list')
  async list(@Query() paginationDto: PaginationDto) {
    return this.client.send({ cmd: 'list' }, { paginationDto });
  }
}
