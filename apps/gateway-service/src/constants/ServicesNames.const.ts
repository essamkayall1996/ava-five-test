export const ServicesNames = Object.freeze({
  USER_SERVICE: {
    NAME: 'USER_SERVICE',
  },
  PRODUCT_SERVICE: {
    NAME: 'PRODUCT_SERVICE',
  },
} as const);
