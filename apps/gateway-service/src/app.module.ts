import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { UserController } from './user/user.controller';
import { AuthModule } from './auth/auth.module';
import { ProductController } from './products/product.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { validate } from './config/config.validation';
import { Envs } from './constants/envs.const';
import { ServicesNames } from './constants/ServicesNames.const';

@Module({
  imports: [
    ConfigModule.forRoot({
      validate: validate,
      isGlobal: true,
      envFilePath: 'gateway-service/.env',
    }),
    ClientsModule.registerAsync([
      {
        name: ServicesNames.USER_SERVICE.NAME,
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.TCP,
          options: {
            host: configService.get<string>(Envs.USER_SERVICE.HOST),
            port: configService.get<number>(Envs.USER_SERVICE.PORT),
          },
        }),
        inject: [ConfigService],
      },
      {
        name: ServicesNames.PRODUCT_SERVICE.NAME,
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.TCP,
          options: {
            host: configService.get<string>(Envs.PRODUCT_SERVICE.HOST),
            port: configService.get<number>(Envs.PRODUCT_SERVICE.PORT),
          },
        }),
        inject: [ConfigService],
      },
    ]),
    AuthModule,
  ],
  controllers: [UserController, ProductController],
})
export class AppModule {}
