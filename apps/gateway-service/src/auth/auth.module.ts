// auth.module.ts

import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: '12345678',
      signOptions: { expiresIn: '3600s' },
    }),
  ],
  exports: [PassportModule, JwtModule],
})
export class AuthModule {}
