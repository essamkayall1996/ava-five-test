import { Type, plainToInstance } from 'class-transformer';
import { IsNumber, IsString, validateSync } from 'class-validator';
import { Envs } from '../constants/envs.const';

class EnvironmentVariables {
  @IsNumber()
  @Type(() => Number)
  [Envs.PORT]!: number;

  @IsString()
  @Type(() => String)
  [Envs.JWT_SECRET]!: string;

  // PRODUCT SERVICE ENV
  @IsString()
  @Type(() => String)
  [Envs.PRODUCT_SERVICE.HOST]!: string;

  @IsNumber()
  @Type(() => Number)
  [Envs.PRODUCT_SERVICE.PORT]!: number;

  // USER SERVICE ENV
  @IsString()
  @Type(() => String)
  [Envs.USER_SERVICE.HOST]!: string;

  @IsNumber()
  @Type(() => Number)
  [Envs.USER_SERVICE.PORT]!: number;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToInstance(EnvironmentVariables, config);

  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }

  return validatedConfig;
}
